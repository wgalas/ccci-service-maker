#!/usr/bin/env node
const rl = require("readline-sync");
const fs = require("fs");
const chalk = require("chalk");
const fse = require("fs-extra");
const path = require("path");
const loading = require("loading-cli");

let service = {
  name: "",
  description: "",
  author: "",
};

function init() {
  try {
    Object.keys(service).forEach((key) => {
      service[key] = rl.question(chalk.green(`${key} => `));
    });

    const pjsonLoad = loading("creating package.json").start();
    fs.readFile(
      path.join(__dirname, "templates/package.json"),
      "utf8",
      (error, content) => {
        if (error) {
          pjsonLoad.fail("failed to create package.json");
          throw error;
        }
        let result = content;
        Object.keys(service).forEach((key) => {
          result = result.replace(`@@${key}@@`, service[key]);
        });
        let projectPath = path.join(process.cwd(), service.name);

        const projectDirLoad = loading(
          `creating project '${projectPath}'`
        ).start();

        fs.mkdir(projectPath, (error) => {
          if (error) {
            projectDirLoad.fail("failed to create project");
            throw error;
          }
          fs.writeFile(
            path.join(projectPath, "package.json"),
            result,
            (error) => {
              if (error) {
                pjsonLoad.fail("failed to create package.json");
                throw error;
              }
              pjsonLoad.succeed("package.json has been created.");
            }
          );
          projectDirLoad.succeed(`project has been created ${projectPath}.`);
        });
        const copyServiceLoad = loading(
          `copying template service to ${projectPath}`
        ).start();
        fse.copy(
          path.join(__dirname, "templates/lib"),
          projectPath,
          (error) => {
            if (error) {
              throw error;
            }
            copyServiceLoad.succeed(
              `template has been copied to ${projectPath}`
            );

            console.log(
              chalk.green(`Done ! cd ${service.name} && npm install`)
            );
          }
        );
      }
    );
  } catch (error) {
    console.log(error.message);
  }
}

init();
