import fs from 'fs';
import path from 'path';
import passport from 'passport';
import ApplicationSettings from './ApplicationSettings.js';

const API_CONTEXT = process.env.API_CONTEXT || 'api'
const PUBLIC_ROUTE = [
    '/api/v1.0/auth',
    '/api/v1.0/admission',
]

export default class RouterMiddleware {

    constructor() { }

    /**
     *
     * @param {Object} app
     * @param {String} folderName
     * @description initialize the routes in a specific directory+
     */
    static init(app, folderName) {
        try {
            fs.readdirSync(folderName).forEach((dir) => {

                var routerDir = path.join(folderName, dir);

                fs.readdirSync(routerDir).forEach((file) => {
                    var fullName = path.join(routerDir, file);
                    if (file.toLowerCase().indexOf('.js')) {

                        let endpoint = '/' + fullName.replace(folderName, API_CONTEXT).replace('.js', '').split('\\').join('/')
                        let routeFile = `../${fullName}`

                        // dynamic import
                        import(routeFile).then(routerClass => {
                            //intialize Router instances
                            let router = new routerClass.default();

                            if (!PUBLIC_ROUTE.includes(endpoint) && (ApplicationSettings.getValue('SECURE_API') || '').toLowerCase() === 'true')
                            // if (!PUBLIC_ROUTE.includes(endpoint))
                            // if(false)
                            {
                                app.use(endpoint,
                                    function (req, res, next) {
                                        passport.authenticate('user-bearer', { session: false }, function (error, user, info) {
                                            if (error) return res.status(error.status || 401).json(error);
                                            if (!user) return res.status(401).json({ message: "Invalid token." })
                                            req.user = user;
                                            next();
                                        })(req, res, next)
                                    },
                                    router.getRoutes());
                            } else {
                                app.use(endpoint, router.getRoutes());
                            }

                            console.log('\x1b[36m', `Initializing endpoint: ${endpoint}`, '\x1b[0m')
                        })
                    }
                })
            });
        } catch (error) {
            console.log('error :>> ', error);
        }

    }

}
