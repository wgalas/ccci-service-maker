import { Op, Sequelize } from "sequelize";

const QUERY_WARN = '\nReplacing of Mongoose Operators ($) can only handle 3 level of nested objects. Please check implementation on /utils/QueryHelper.js -> static convertQuery()\n';
const ASSOCIATIONS_WARN = '\nError in handleAssociations: fields or associations is null.\n'
export default class QueryHelper {

    /**
     * @description convert query to sequelize' where
     * @param {*} query 
     */
    static where(query) {
        query = this.convertQuery(query)
        return query
    }

    /**
     * @description convert populate to sequelize' includes
     * @param {*} includes 
     * @param {*} fields 
     */
    static includes(includes, fields) {
        let associations = []
        // let defaultWhere = (fields || []).includes("isActive") ? { isActive: true } : {}
        let defaultWhere = {}
        if (includes && includes.length > 0) {
            includes.forEach(include => {
                console.log('###include :>> ', typeof include);
                //for nested includes
                if ((include.path && include.path.indexOf('.') > -1)
                    || (typeof include == "string" && include.indexOf('.') > -1)) {
                    let records = []
                    let nested = include.path ? include.path.split('.') : include.split('.')
                    //loop nested keys starting from the deepest
                    for (let i = nested.length - 1; i >= 0; i--) {
                        records.push({ include: [{ association: nested[i] }] })
                    }
                    //insert deeper include to higher hierarchy
                    let temp;
                    for (let i = 0; i < records.length - 1; i++) {
                        records[i + 1].include[0].include = records[i].include[0]
                        temp = records[i + 1].include[0]
                    }
                    //add to association
                    associations.push(temp)
                } else {
                    let translateAssociateQuery = { association: include.path || include };
                    if (typeof include == "object") {
                        if (include.required) translateAssociateQuery.required = true;
                        if (include.where) translateAssociateQuery.where = this.where(include.where);
                        if (include.sort) translateAssociateQuery.order = this.sort(include.sort);
                        if (include.limit) translateAssociateQuery.limit = this.limit(include.limit);
                        if (include.fields) translateAssociateQuery.attributes = this.select(include.fields, null)
                    }
                    associations.push(translateAssociateQuery)
                }
            })
        }
        return associations
    }

    /**
     * @description convert populate to sequelize' includes
     * @param {*} includes 
     */
    static offset(page) {
        return page
    }

    /**
     * @description convert populate to sequelize' includes
     * @param {*} includes 
     */
    static limit(limit) {
        return limit
    }

    /**
     * @description convert populate to sequelize' includes
     * @param {*} includes 
     */
    static select(fields, modelFields) {
        let selectFields = []
        if (fields) {
            let _fields = [];
            if (fields.indexOf(',') > -1) _fields = fields.split(',');
            else if (fields.indexOf(' ') > -1) _fields = fields.split(' ');
            else _fields = [fields];
            _fields.forEach(field => {
                if (modelFields && modelFields.includes(field + "Id")) {
                    selectFields.push(field + "Id")
                } else selectFields.push(field)
            })
        }
        if (!selectFields.includes("_id")) selectFields.push("_id"); // selecting _id by default
        return selectFields
    }

    /**
     * @description convert sorting syntax
     * @param {*} fields 
     */
    static sort(fields) {
        if (fields == 'random') return Sequelize.fn('random')
        let conditions = []
        if (fields) {
            let keys = Object.keys(fields)
            keys.forEach(key => {
                if (fields[key] == -1) fields[key] = 'DESC'
                if (fields[key] == 1) fields[key] = 'ASC'
                conditions.push([key, fields[key]])
            })
        }
        return conditions
    }

    /**
     * @replace Convert Operator keys
     * @param {*} query 
     */
    // static convertQuery(query) {
    //     // TODO: improve looping syntax
    //     // current implem can only handle 3 nested objects
    //     try {
    //         Object.keys(query).forEach(key => {
    //             if (typeof query[key] === "object" && !Array.isArray(query[key]) && query[key] !== null) {
    //                 //Object (re-iterate)
    //                 Object.keys(query[key]).forEach(key2 => {
    //                     if (typeof query[key][key2] === "object" && !Array.isArray(query[key][key2]) && query[key][key2] !== null) {
    //                         if (typeof query[key] === "object" && !Array.isArray(query[key]) && query[key] !== null) {
    //                             console.warn('\x1b[33m', QUERY_WARN, '\x1b[0m');
    //                         } else {
    //                             Object.keys(query[key][key2]).forEach(key3 => {
    //                                 this.replace(query[key][key2], key3)
    //                             })
    //                         }
    //                     } else {
    //                         this.replace(query[key], key2)
    //                     }
    //                 })
    //             } else {
    //                 this.replace(query, key)
    //             }
    //         })
    //     } catch (error) {
    //         console.log('error :>> ', error);
    //     }

    //     return query;
    // }
    static convertQuery(query) {
        // TODO: iteration
        console.log('query :>> ', JSON.stringify(query));
        if (!query) return query;
        if (Array.isArray(query))
            query.forEach(subQuery => {
                this.convertQuery(subQuery);
            })
        else if (typeof query == "object" && !Array.isArray(query))
            Object.keys(query).forEach(key => {
                let nested = query[key]
                this.replace(query, key);
                this.convertQuery(nested);
            })
        console.log('query :>> ', JSON.stringify(query));
        return query;
    }

    /**
     * @description WIP - other approach for replacement. 
     *              1. iterate through the list of Op
     *              2. scan the query and search for the matching object
     *              3. replace the key with the correct Op
     * @param {*} query 
     * @param {*} lookupKey 
     */
    static traverse(query, lookupKey) {
        var result = false;
        if (query instanceof Array) {
            for (var i = 0; i < query.length; i++) {
                result = getObject(query[i]);
                if (result) {
                    break;
                }
            }
        } else {
            for (var prop in query) {
                if (prop === lookupKey) {
                    return query;
                }
                if (theObject[prop] instanceof Object || theObject[prop] instanceof Array) {
                    result = getObject(theObject[prop]);
                    if (result) {
                        break;
                    }
                }
            }

        }
        return result;

    }

    /**
     * @description Find and replace mongo operators
     * @param {Object} obj 
     * @param {field} key 
     */
    static replace(obj, key) {
        // console.log('obj[key].replace(/\.\*/g,'%') :>> ', obj[key].replace(/\.\*/g,'%'));
        let replacements = {
            $not: { [Op.not]: obj[key] },
            $and: { [Op.and]: obj[key] },
            $or: { [Op.or]: obj[key] },
            $ne: { [Op.ne]: obj[key] },
            $nin: { [Op.notIn]: obj[key] },
            $in: { [Op.in]: obj[key] },
            $lt: { [Op.lt]: obj[key] },
            $lte: { [Op.lte]: obj[key] },
            $gt: { [Op.gt]: obj[key] },
            $gte: { [Op.gte]: obj[key] },
            $contains: { [Op.contains]: obj[key] },
            $elemMatch: { [Op.contains]: !Array.isArray(obj[key]) ? [obj[key]] : obj[key] },
            $regex: {
                [Op.iLike]: obj[key]
                    && typeof obj[key] === 'string'
                    && key !== 'token' ? obj[key].replace(/\.\*/g, '%') : '%'
            }
        }
        //remove unused operators
        if ('$options' === key) {
            delete obj[key]
        }

        let rep = replacements[key]
        if (rep) {

            Object.assign(obj, rep)
            delete obj[key]
        }
    }

    static handleAssociations(translated, fields, associations) {
        // TODO: improve looping syntax
        // current implem can only handle 1 nested objects
        console.log('associations :>> ', associations);
        if (!fields || !associations) console.warn('\x1b[33m', ASSOCIATIONS_WARN, `\n fields: ${!!fields}, associations: ${!!associations}`, '\x1b[0m');
        if (translated && translated.where) {
            Object.keys(translated.where).forEach(key => {
                if (fields.includes(key + "Id")) {
                    translated.where[key + "Id"] = translated.where[key];
                    delete translated.where[key];
                } else if (associations.includes(key)) {
                    if (!translated.include) translated.include = [];
                    let where = {
                        _id: translated.where[key]
                    }
                    if (typeof translated.where[key] == 'object') where = translated.where[key]
                    translated.include.push({
                        association: key,
                        where
                    })
                    delete translated.where[key];
                }
            })
        }
        return translated;
    }

}