export default class Mixins {
  static formatDate(date, type) {
    if (!date) return date;
    var dt = new Date(date);

    if (!type) {
      type = {
        year: "numeric",
        month: "long",
        day: "2-digit",
      };
    } else if (type === "datetime") {
      type = {
        hour12: true,
        year: "numeric",
        month: "long",
        day: "2-digit",
        hour: "2-digit",
        minute: "2-digit",
      };
    } else if (type === "time") {
      type = {
        hour12: true,
        hour: "2-digit",
        minute: "2-digit",
      };
    } else if (type === "time24") {
      type = {
        hour12: false,
        hour: "2-digit",
        minute: "2-digit",
        second: "2-digit",
      };
    } else if (type === "YYYY-MM-DD") {
      type = {
        year: "numeric",
        month: "2-digit",
        day: "2-digit",
      };
      // return dt.toLocaleString('fr-CA', { year: 'numeric', month: '2-digit', day: '2-digit' });
    }
    type.timeZone = "Asia/Manila";
    return dt.toLocaleString("en-US", type);
  }

  static formatCurrency(amount, no_sign) {
    if (!amount || isNaN(amount)) return no_sign ? "0.00" : "₱ 0.00";
    var parts = parseFloat(amount).toFixed(2).toString().split(".");
    parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    return `${no_sign ? "" : "₱ "}${parts.join(".")}`;
  }

  static getEmployeeName(emp) {
    let fullname = "-";
    if (emp) {
      if (emp.lastName && emp.firstName)
        fullname = `${emp.lastName}, ${emp.firstName || ""} ${
          emp.middleName || ""
        }`;
    }
    return fullname;
  }

  static displaySchedules(schedules) {
    console.log("schedules :>> ", schedules);
    let _schedules = [];
    schedules.forEach((sched) => {
      let i = (_schedules || []).findIndex(
        (v) => v.timeFrom == sched.timeFrom && v.timeTo == sched.timeTo
      );
      if (i > -1) {
        _schedules[i].day += "" + sched.day;
      } else {
        _schedules.push({
          day: sched.day,
          timeFrom: sched.timeFrom,
          timeTo: sched.timeTo,
        });
      }
    });
    return _schedules
      .map((v) => `${v.day} (${v.timeFrom} - ${v.timeTo})`)
      .join("<br>");
  }

  static formatTIN(tin) {
    if (tin) {
      tin = tin.toString().replace(/-/g, "");
      tin = tin.substring(0, 13);
    }
    if (tin && tin.length > 3 && tin.length < 7) {
      tin = `${tin.substring(0, 3)}-${tin.substring(3, tin.length)}`;
    } else if (tin && tin.length > 6 && tin.length < 10) {
      tin = `${tin.substring(0, 3)}-${tin.substring(3, 6)}-${tin.substring(
        6,
        tin.length
      )}`;
    } else if (tin && tin.length > 9) {
      tin = `${tin.substring(0, 3)}-${tin.substring(3, 6)}-${tin.substring(
        6,
        9
      )}-${tin.substring(9, tin.length)}`;
    }
    return tin;
  }

  static formatGSIS(tin) {
    if (tin) {
      tin = tin.toString().replace(/-/g, "");
      tin = tin.substring(0, 13);
    }
    if (tin && tin.length > 3 && tin.length < 7) {
      tin = `${tin.substring(0, 3)}-${tin.substring(3, tin.length)}`;
    } else if (tin && tin.length > 6 && tin.length < 10) {
      tin = `${tin.substring(0, 3)}-${tin.substring(3, 6)}-${tin.substring(
        6,
        tin.length
      )}`;
    } else if (tin && tin.length > 9) {
      tin = `${tin.substring(0, 3)}-${tin.substring(3, 6)}-${tin.substring(
        6,
        9
      )}-${tin.substring(9, tin.length)}`;
    }
    return tin;
  }

  static formatPhilhealth(tin) {
    if (tin) {
      tin = tin.toString().replace(/-/g, "");
      tin = tin.substring(0, 13);
    }
    if (tin && tin.length > 2 && tin.length < 12) {
      tin = `${tin.substring(0, 2)}-${tin.substring(2, tin.length)}`;
    } else if (tin && tin.length > 11) {
      tin = `${tin.substring(0, 2)}-${tin.substring(2, 11)}-${tin.substring(
        11,
        tin.length
      )}`;
    }
    return tin;
  }

  static formatHDMF(tin) {
    if (tin) {
      tin = tin.toString().replace(/-/g, "");
      tin = tin.substring(0, 12);
    }
    if (tin && tin.length > 4 && tin.length < 8) {
      tin = `${tin.substring(0, 4)}-${tin.substring(4, tin.length)}`;
    } else if (tin && tin.length > 7 && tin.length < 13) {
      tin = `${tin.substring(0, 4)}-${tin.substring(4, 8)}-${tin.substring(
        8,
        tin.length
      )}`;
    }
    return tin;
  }

  static getNumberIteration(num, totalIteration = 5) {
    if (!num && num != 0) return "";
    let numLen = num.toString().length;
    if (numLen >= totalIteration) return num.toString();
    let iterated = "";
    for (let i = 0; i < totalIteration - numLen; i++) iterated += "0";
    return iterated + num.toString();
  }
}
