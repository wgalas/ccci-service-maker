import DatabaseService from "../models/index.js";
import * as dotenv from "dotenv";
dotenv.config();

const APP_PORT = process.env.APP_PORT || process.env.npm_config_port || 5000;
const SYNC_DB = process.env.SYNC_DB || process.env.npm_config_sync || false;
const SYNC_DB_NAME =
  process.env.SYNC_DB_NAME || process.env.npm_config_db || "sequelize";
const SYNC_DB_FORCE =
  process.env.SYNC_DB_FORCE || process.env.npm_config_force || false;

export default class InitializeApp {
  static async init(app) {
    try {
      let db = await DatabaseService.init();
      if (SYNC_DB) {
        console.log(
          `##### db: ${SYNC_DB_NAME} is force: ${SYNC_DB_FORCE} #####`
        );
        await db[SYNC_DB_NAME].sync({ force: SYNC_DB_FORCE });
        console.info(
          "\x1b[33m ############################ Done data syncing ############################ \x1b[0m"
        );
      } else {
        await (await import("./ApplicationSettings.js")).default.init();
        (await import("./RouterMiddleware.js")).default.init(app, "routes");
        await import("./OAuth.js");
        (await import("./Mailer.js")).default.init();
        (await import("./Publisher.js")).default.init();
        app.listen(APP_PORT, () => {
          console.log("Running local in port: ", APP_PORT);
          console.log(
            "\x1b[36m",
            "Server Local Time: ",
            new Date().toLocaleString("en-US", {
              hour12: true,
              year: "numeric",
              month: "long",
              day: "2-digit",
              hour: "2-digit",
              minute: "2-digit",
              timeZone: "Asia/Manila",
            }),
            "\x1b[0m"
          );
        });
      }
    } catch (error) {
      console.error("Error in initialization...");
      console.error(error);
    }
  }
}
