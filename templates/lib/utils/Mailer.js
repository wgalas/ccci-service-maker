import nodemailer from 'nodemailer';
import handlebars from 'handlebars';
import fs from 'fs';
import ApplicationSettings from './ApplicationSettings.js';

const LOCAL_DIR = process.env.LOCAL_DIR || "";

Date.prototype.addHours = function (hours) {
    this.setTime(this.getTime() + (hours * 60 * 60 * 1000));
    return this;
}

var transporter;

export default class Mailer {
    static async init() {
        transporter = nodemailer.createTransport({
            service: 'gmail',
            auth: {
                user: ApplicationSettings.getValue("SENDER_EMAIL_ACCOUNT"), //process.env.EMAIL_ACCOUNT
                pass: ApplicationSettings.getValue("SENDER_EMAIL_PASSWORD") //process.env.EMAIL_PASSWORD
            }
        });
    }

    /**
     *
     * @param {*} mailOptions
     */
    static sendMail(mailOptions) {
        transporter.sendMail(mailOptions, (err, info) => {
            if (err)
                console.error(err)
            else
                console.log(info)
        });
    }

    /**
     *
     * @param {String} path
     * @param {function} callback
     */
    static createEmailTemplate(path) {
        console.log(`PATH::: ${LOCAL_DIR}${path}`)
        return new Promise((resolve, reject) => {
            fs.readFile(`${LOCAL_DIR}${path}`, { encoding: 'utf-8' }, (err, html) => {
                if (err) {
                    reject(err)
                }
                else {
                    resolve(html);
                }
            });
        })

    };

    static createEmailTemplateV2(path) {
        // console.log(`PATH::: ${LOCAL_DIR}${path}`)
        return new Promise((resolve, reject) => {
            fs.readFile(`./templates/${path}`, { encoding: 'utf-8' }, (err, html) => {
                if (err) {
                    reject(err)
                }
                else {
                    resolve(html);
                }
            });
        })

    };

    /**
     * @returns {Promise}
     * @param {String} to
     * @param {String} template_id
     * @param {Object} dynamic_template_data
     */
    static sendEmail(to, template_id, dynamic_template_data, subject) {
        return new Promise((resolve, reject) => {
            if (dynamic_template_data) {
                if (dynamic_template_data.name) dynamic_template_data.name = capitalizeName(dynamic_template_data.name)
                dynamic_template_data.contact_no = ApplicationSettings.getValue("CONTACT_NO");
                dynamic_template_data.contact_email = ApplicationSettings.getValue("SENDER_EMAIL");
            }

            var _self = this;
            this.createEmailTemplate(ApplicationSettings.getValue(template_id))
                .then(html => {
                    var template = handlebars.compile(html);
                    const mailOptions = {
                        from: ApplicationSettings.getValue("SENDER_EMAIL"), // sender address
                        to, // list of receivers
                        subject: subject ? subject : 'Chizmis Notification', // Subject line
                        html: template(dynamic_template_data)
                    };
                    _self.sendMail(mailOptions)
                    resolve(true)
                })
                .catch(err => {
                    console.error("ERROR", err)
                    reject(err)
                })
        })
    }

    static sendEmailV2(to, template_path, dynamic_template_data, subject) {
        return new Promise((resolve, reject) => {
            if (dynamic_template_data) {
                if (dynamic_template_data.name) dynamic_template_data.name = capitalizeName(dynamic_template_data.name)
                dynamic_template_data.contact_no = ApplicationSettings.getValue("CONTACT_NO");
                dynamic_template_data.contact_email = ApplicationSettings.getValue("SENDER_EMAIL");
            }

            var _self = this;
            this.createEmailTemplateV2(template_path)
                .then(html => {
                    var template = handlebars.compile(html);
                    const mailOptions = {
                        from: ApplicationSettings.getValue("SENDER_EMAIL") || 'hris.supp@gmail.com', // sender address
                        to, // list of receivers
                        subject: subject ? subject : 'UIS Notification', // Subject line
                        html: template(dynamic_template_data)
                    };
                    _self.sendMail(mailOptions)
                    resolve(true)
                })
                .catch(err => {
                    console.error("ERROR", err)
                    reject(err)
                })
        })
    }
}

function capitalizeName(name) {
    if (!name) return name;
    let names = name.split(' '); // Split by space
    let capitalized_name = names.map(v => (v.charAt(0).toUpperCase() + v.slice(1))).join(" ") // Uppercase the first letter then Join again by space
    return capitalized_name;
}
