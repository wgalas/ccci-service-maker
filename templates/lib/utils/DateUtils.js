export default class DateUtils{

    /**
     * @description Get the duration between dates
     * @param {Date} date1 
     * @param {Date} date2 
     * @returns {Number} duration in hours
     */
    static getDuration(date1, date2, strict){
        let duration = null;
        try {
            if(strict){
                duration = (date1.getTime() - date2.getTime()) / 3600000
            }else{
                duration = Math.abs(date1.getTime() - date2.getTime()) / 3600000
            }
            
        } catch (error) {
            console.log('error :>> ', error);
        }finally{
            return duration
        }
    }
}