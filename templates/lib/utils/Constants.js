export default {
    email_regex: /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/,
    not_match_password_message: "Password should contain at least 1 Uppercase, 1 Lowercase, 1 Digit, 1 Special Character and 8 Characters length.",
    password_regex: /(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*\W)[A-Za-z\d\W]{8,}/
}