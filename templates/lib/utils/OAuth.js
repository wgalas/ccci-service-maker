import passport from "passport";
import PassportLocal from "passport-local";
import PassportHttpBearer from "passport-http-bearer";
import jwt from "jsonwebtoken";
import ApplicationSettings from "./ApplicationSettings.js";
import axios from "axios";

const LocalStrategy = PassportLocal.Strategy;
const BearerStrategy = PassportHttpBearer.Strategy;

import Model from "../models/index.js";
const { users } = Model.getDb();
const UserModel = new users();

/**
 * @description Validate Tokenized Requests
 */
passport.use(
  "user-bearer",
  new BearerStrategy(async function (token, done) {
    try {
      let user = await UserModel.findOne({ token }).exec();
      if (!user) {
        return done(null, false);
      }
      if (!user.isActive) {
        return done({ message: "User is deactivated." }, false);
      }
      user = JSON.parse(JSON.stringify(user));

      return done(null, user, { scope: "all" });
    } catch (error) {
      console.error("### Error in user-bearer >> ", error);
      return done(error);
    }
  })
);

/**
 * @description authentication using passport's local strategy
 */
passport.use(
  "user-login",
  new LocalStrategy(
    {
      usernameField: "username",
      passwordField: "password",
      passReqToCallback: true,
    },
    async (req, username, password, done) => {
      try {
        if (!username || !password)
          return done({ message: "Invalid Credentials", statusCode: 401 });

        let user = await UserModel.findOne({ username }).exec();
        if (!user) return done({ message: "No user found", statusCode: 401 });

        if (user.lockedDate)
          return done({
            message: "Your account is currently locked.",
            statusCode: 401,
          });

        let isValid = await user.isValid(password);
        if (!isValid) {
          let loginAttempts = (user.loginAttempts || 0) + 1;
          let failedPayload = { loginAttempts };
          let isLocked =
            loginAttempts >=
            (ApplicationSettings.getValue("MAX_LOGIN_ATTEMPTS") || 3);
          if (isLocked) failedPayload.lockedDate = new Date();
          UserModel.findByIdAndUpdate(user._id, failedPayload);
          return done({
            message: isLocked
              ? "Your account has been locked."
              : "Invalid Credentials",
            statusCode: 401,
          });
        }

        // check user access
        let access = "";
        if (user.accountType && user.accountType === "STUDENT")
          access = ApplicationSettings.getValue("STUDENT_WHITELIST");
        else access = ApplicationSettings.getValue("ADMIN_WHITELIST");
        access = access.split(",");

        if (!access.includes(req.headers.origin))
          return done({ message: "User not Allowed", statusCode: 401 });

        const token = jwt.sign(
          {
            userId: user._id,
            date: new Date(),
          },
          ApplicationSettings.getValue("JWT_SECRET_TOKEN"),
          { expiresIn: "30m" }
        );

        await UserModel.findByIdAndUpdate(user._id, {
          token,
          loginAttempts: 0,
          lockedDate: null,
          lastLogin: new Date(),
        });

        done(null, { token, _id: user._id }, { scope: "read" });
      } catch (error) {
        console.error("### Error in user-login >> ", error);
        done(error);
      }
    }
  )
);

passport.use(
  "token-exchange",
  new LocalStrategy(
    {
      usernameField: "username",
      passwordField: "sub",
      passReqToCallback: true,
    },
    async (req, username, sub, done) => {
      try {
        if (!username || !sub)
          return done({ message: "Invalid Credentials", statusCode: 401 });

        let userInfo = await axios.get(
          `${
            process.env.VUE_APP_SSO_URL ||
            "http://kc.rsu.beacon-solutions.com:8080"
          }/auth/realms/${
            process.env.VUE_APP_SSO_REALM || "RSU"
          }/protocol/openid-connect/userinfo`,
          {
            headers: {
              Authorization: req.headers.authorization,
            },
          }
        );
        if (
          !userInfo ||
          !userInfo.data ||
          userInfo.data.preferred_username != username ||
          userInfo.data.sub != sub
        )
          return done({ message: "Invalid Credentials", statusCode: 401 });

        let user = await UserModel.findOne({ username })
          .populate([
            "student.course",
            "student.curriculum",
            "student.campus",
            "role.permissions.module",
            "employee.campus",
          ])
          .exec();
        if (!user) return done({ message: "No user found", statusCode: 401 });

        const token = jwt.sign(
          {
            userId: user._id,
            date: new Date(),
          },
          ApplicationSettings.getValue("JWT_SECRET_TOKEN"),
          { expiresIn: "30m" }
        );

        await UserModel.findByIdAndUpdate(user._id, {
          token,
          loginAttempts: 0,
          lockedDate: null,
          lastLogin: new Date(),
        });

        user = JSON.parse(JSON.stringify(user));
        user.token = token;
        if (user.accountType && user.accountType === "STUDENT")
          user.profile = user.student;
        else user.profile = user.employee;
        user.campus = user.profile.campus;
        user.password = undefined;

        // get default settings
        let academicYear = await AcademicYearModel.findOne({
          default: true,
          isActive: true,
        }).exec();
        let semester = await SemesterModel.findOne({
          default: true,
          isActive: true,
        }).exec();
        user.defaultAcademicYear = academicYear;
        user.defaultSemester = semester;

        done(null, user, { scope: "read" });
      } catch (error) {
        console.error("### Error in token-exchange >> ", error);
        done(error);
      }
    }
  )
);
