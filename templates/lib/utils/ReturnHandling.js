export default class ReturnHandling{
    /**
     * SUBJECT TO CHANGE -- Matthew Godez | June 02, 2021
     * @param {*} err 
     * @returns error mapping
     */
     static getError(err){
        let message = 'We\'ve encountered some issues as of the moment. Please try again later. Thank you!';
        if(err){
            if(typeof err.errors !== 'undefined'){

                //customize message
                //throw single error message per request
                let error = err.errors[0]
                message =  err.errors.map(e => e.message).toString()
            }
            else if(typeof err.message !== 'undefined'){
                message = err.message
            }
        }

        return message;
    }

    static retHandle(code, data, message){
        return JSON.parse(JSON.stringify({
            "code": code,
            "data": data,
            "message": message,
        }));
    }

    static retError(err){
        console.error('err :>> ', JSON.stringify(err));
        return JSON.parse(JSON.stringify({
            "code": err ? err.status || err.statusCode || 400 : 400,
            "data": [],
            "message": this.getError(err),
            "error":err
        }));
    }
}