import Model from "../models/index.js";
const { integration_log: IntegrationLogModel } = Model.getDb();

//allowable query keys
const INCLUDE = "include";
const LIMIT = "limit";
const PAGE = "page";
const ORDER = "order";
const SEARCH = "search";
const DATE_RANGE = "date_range";
const KEYWORD = "keyword";

const QUERIES = [INCLUDE, LIMIT, PAGE, ORDER];

//defaults
const DEFAULT_LIMIT = 10;
const DEFAULT_PAGE = 0;
const DEFAULT_ORDER = { _id: -1 };

/**
 * @description Controller Class
 *
 */
export default class BaseController {
  /**
   * @description constructor method
   * @param {} model
   */
  constructor(model) {
    if (model) {
      this.model = new model();
    }
    // this.model.setAssociations(model.associations);
  }

  /**
   * @description Find all records in the DB
   * @param {HttpRequest} req
   * @returns {Object} response
   * paginate = Boolean (default:true)
   * page = Number
   * limit = Number
   * search = field:value - separated by comma(,) (For wildcard search) field:.*value.*
   * sort = field:(asc/desc) - separated by comma(,)
   * includes = string - separated by comma(,) can also add subdocuments (Ex. employee.details)
   * fields = string - separated by comma(,)
   */
  async find(data) {
    try {
      let searchOptions = this.constructSearch(data);
      let sort = this.constructSort(data.query.sort);
      let limit =
        data.query.paginate == "false"
          ? 0
          : data.query.limit
          ? data.query.limit
          : 10;
      let page = data.query.page || 1;
      if (data.query.exclude) {
        let exclude = data.query.exclude.split(",");
        searchOptions._id = { $nin: exclude };
      }
      let results = await this.model
        .find(searchOptions)
        .populate(this.constructPopulate(data.query.includes))
        .select(data.query.fields)
        .limit(limit)
        .skip((page - 1) * limit)
        .sort(sort)
        .exec();
      return results;

      // if (data.query.paginate == false || data.query.paginate == "false") return { rows: await _rows.exec() };
      // else {
      //     let page = !isNaN(data.query.page) ? parseInt(data.query.page) : 1;
      //     let limit = !isNaN(data.query.limit) ? parseInt(data.query.limit) : 10;

      //     let rows = await _rows.skip((page - 1) * limit).limit(limit).exec();

      //     let count = await this.model.count(searchOptions);
      //     return { rows, count };
      // }
    } catch (error) {
      return this.createResponse(null, error);
    }
  }

  constructSort = (sortBy) => {
    if (!sortBy) return { createdDate: -1 };
    let sort = {};
    sortBy.split(",").forEach((srt) => {
      let pair = srt.split(":");
      sort[pair[0]] = pair[1];
    });
    return sort;
  };

  constructPopulate = (docs) => {
    if (!docs) return "";
    return docs.split(",").map((path) => {
      if (path.indexOf("!") > -1)
        return { path: path.replace("!", ""), required: true };
      return path;
    });
  };

  constructMongoosePopulate = (docs) => {
    if (!docs) return "";
    let _docs = docs.split(",");
    if (docs.indexOf(".") == -1) return _docs;
    var populate = [];
    _docs = _docs.sort((a, b) => {
      let countA = (a.match(/\./g) || []).length,
        countB = (b.match(/\./g) || []).length;
      return countA - countB;
    });
    // todo constructSubPopulate
    _docs.forEach((doc) => {
      if (doc.indexOf(".") == -1) populate.push({ path: doc });
      else {
        let pair = doc.split(".");
        let i = populate.findIndex((v) => v.path == pair[0]);
        if (i == -1) populate.push({ path: doc });
        else {
          if (!populate[i].populate) populate[i].populate = [];
          populate[i].populate.push(pair[1]);
        }
      }
    });
    return populate;
  };

  /**
   * @description Find record by id
   * @param {HttpRequest} req
   * @returns {Object} response
   */
  async findById(req) {
    try {
      let results = await this.model
        .findById(req.params.id)
        .populate(this.constructPopulate(req.query.includes))
        .select(req.query.fields)
        .exec();
      return this.createResponse(results);
    } catch (error) {
      return this.createResponse(null, error);
    }
  }

  /**
   * @description Create single record
   * @param {HttpRequest} req
   * @returns {Object} response
   */
  async create(req) {
    try {
      let data = req.body;
      if (req.user) data.createdById = req.user._id;
      let results = await this.model.create(data);

      this.publish("create", results);
      this.log(req, "ADD", null);

      return this.createResponse(results);
    } catch (error) {
      return this.createResponse(null, error);
    }
  }

  /**
   * @description Update record defined by the id in the request params
   * @param {HttpRequest} req
   * @returns
   */
  async update(req) {
    try {
      let id = req.params.id;
      let data = req.body;
      if (req.user) data.modifiedById = req.user._id;
      data.modifiedDate = new Date();
      let results = await this.model.findOneAndUpdate({ _id: id }, data, {
        new: true,
      });

      this.publish("update", results);
      this.log(req, "UPDATE", null);
      return this.createResponse(results);
    } catch (error) {
      return this.createResponse(null, error);
    }
  }

  /**
   * @description private method to parse search queries
   * @param {HttpRequest} req
   * @returns {Object} search
   */
  constructSearch = (req) => {
    let search = {};
    let fields = req.query[SEARCH];

    try {
      if (fields) {
        fields = fields.split(",");
        fields.forEach((field) => {
          if (field.indexOf("|") > -1) {
            if (!search.$or) search.$or = [];
            let orFields = field.split("|");
            orFields.forEach((orField) => {
              let _field = this.getQueryField(orField);
              if (_field) search.$or.push(_field);
            });
          } else {
            let _field = this.getQueryField(field);
            search = { ...search, ..._field };
          }
        });
      }

      let date_ranges = req.query[DATE_RANGE];
      if (date_ranges) {
        date_ranges = date_ranges.split(",");
        date_ranges.forEach((date_range) => {
          let pair = date_range.split(":");
          let date_query = {};
          if (pair[1])
            date_query.$gte = new Date(pair[1] + "T00:00:00.000+08:00");
          if (pair[2])
            date_query.$lt = new Date(pair[2] + "T23:59:59.000+08:00");
          search[pair[0]] = date_query;
        });
      }
    } catch (error) {
      console.error(error);
    } finally {
      return search;
    }
  };

  getQueryField = (field) => {
    let pair = field.split(":");
    let dataType = pair[2];
    let refId = pair[0] + "Id";
    let fieldKey = null,
      fieldValue = null;
    let attributes = this.model ? this.model.getFields() : null;

    if (attributes && attributes.includes(refId)) {
      fieldKey = refId;
      fieldValue = pair[1];
    } else {
      // variable:value:type
      switch (dataType) {
        case "Date": // value should be formatted as YYYY-MM-DD
          // search[pair[0]] = {
          //     $gte: new Date(pair[1] + "T00:00:00.000+08:00"),
          //     $lt: new Date(pair[1] + "T23:59:59.000+08:00")
          // }
          fieldKey = pair[0];
          fieldValue = {
            $gte: new Date(pair[1] + "T00:00:00.000+08:00"),
            $lt: new Date(pair[1] + "T23:59:59.000+08:00"),
          };
          break;

        case "Boolean":
          // search[pair[0]] = (pair[1] == "true");
          fieldKey = pair[0];
          fieldValue = pair[1] == "true";
          break;

        default:
          if (pair[1]) {
            if (pair[1].startsWith(".*") && pair[1].endsWith(".*")) {
              // wildcard
              fieldKey = pair[0];
              fieldValue = { $regex: pair[1], $options: "i" };
              // search[pair[0]] = { $regex: pair[1], $options: 'i' }
            } else if (!isNaN(pair[1])) {
              // integer
              // search[pair[0]] = parseInt(pair[1])
              fieldKey = pair[0];
              fieldValue = parseInt(pair[1]);
            } else {
              // id/fixed/whole word
              // search[pair[0]] = pair[1];
              fieldKey = pair[0];
              fieldValue = pair[1];
            }
          } else return null;
      }
    }
    return { [fieldKey]: fieldValue == "null" ? null : fieldValue };
  };

  /**
   * @description private method to parse option queries
   * @param {HttpRequest} req
   * @returns {Object} options
   */
  constructOptions = (req) => {
    let options = {
      where: {},
      pagination: {},
      sorting: {},
    };
    try {
      QUERIES.forEach((key) => {
        let query = req.query[key];
        if (query) {
          if (query.includes(",")) query = query.split(",");
          options[key] = query;
        }
      });

      //format order
      if (options[ORDER]) {
        let key = options[ORDER][0];
        let value = options[ORDER][1];
        options.sorting[key] = value;
      } else {
        options.sorting = DEFAULT_ORDER;
      }

      //set limits
      if (!options[LIMIT]) {
        options[LIMIT] = DEFAULT_LIMIT;
      } else if (req[LIMIT].toUpperCase() === "ALL") {
        delete options[LIMIT];
      }

      //set offset
      if (!options[PAGE]) {
        options[PAGE] = DEFAULT_PAGE;
      } else {
        let page = options[PAGE] - 1 < 0 ? 0 : options[PAGE] - 1;
        options.offset = page * options[LIMIT];
      }

      options.pagination = {
        PAGE: options[PAGE],
        LIMIT: options[LIMIT],
      };

      //add search
      options.where = this.constructSearch(req);
    } catch (error) {
      console.error(error);
    } finally {
      return options;
    }
  };

  /**
   * @description parse controller response
   * @param {*} results
   * @param {*} error
   * @returns
   */
  createResponse = (results, error) => {
    let response = {};
    try {
      //for DEBUGGING
      if (error) {
        console.error(error);
        response.message = this.getError(error);
        response.error = error;
        response.status = error.status || (error.errors ? 422 : 500);
      } else if (results && results.rows && results.count) {
        response.data = results.rows;
        response.count = results.count;
      } else {
        response.data = results;
      }
    } catch (error) {
      console.error(error);
    }
    return response;
  };

  /**
   * @description parse error message
   * @param {*} err
   * @returns
   */
  getError = (err) => {
    let message =
      "We've encountered some issues as of the moment. Please try again later. Thank you!";
    if (err) {
      if (err.errors && typeof err.errors == "object") {
        let _err = err.errors[Object.keys(err.errors)[0]];
        message =
          _err.kind == "required" ? `${_err.path} is required.` : _err.message;
      } else if (typeof err.errors !== "undefined") {
        let error = err.errors[0];
        message = err.errors.map((e) => e.message).toString();
      } else if (typeof err.message !== "undefined") {
        message = err.message;
      }
    }

    return message;
  };

  async createIntegrationLogger(model, method, data) {
    // create integration logger instance
    let _model = model.name;
    let il = await IntegrationLogModel.create({
      method,
      load: data,
      model: _model,
      bound: "OUT",
      status: "PROCESSING",
    });

    return il;
  }

  publishToLMS(name, data) {
    LMSPublisher.publish(name, data);
  }

  publishToHRIS(name, data) {
    HRISPublisher.publish(name, data);
  }
  consoleLog(...text) {
    if (false) {
      console.log(text);
    }
  }
}
