var params = [];
import Model from "../models/index.js";
const SettingsModel = Model.getDb().settings;

export default class ApplicationSettings {
  static async init() {
    /****** Setup Application Variables ******/
    let settings_params = await new SettingsModel()
      .find({ status: true })
      .lean()
      .exec();

    this.setApplicationVariables(
      settings_params ? settings_params.rows || {} : {}
    );
  }

  static setApplicationVariables(parameters) {
    params = parameters;
  }

  static getApplicationVariables() {
    return params;
  }

  static getValue(key) {
    var value = "";
    for (var i = 0; i < params.length; i++) {
      var rec = params[i];
      if (rec["code"] === key) {
        value = rec["value"];
        break;
      }
    }
    return value;
  }
}
