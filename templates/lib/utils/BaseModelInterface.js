/**
 * @description BaseModel suggested implementation
 * @description method chaining: 
 *                  -> call instance find method 
 *                  -> put query to instance variable
 *                  -> return object instance
 *                  -> invoke chainned method
 *                  -> translate query
 *                  -> call STATIC find method
 *                  -> call super find method (Sequelize)
 */
export default class ModelClass {
    //instance variables
    query;
    includeFields;
    sortCondition;
    limitOption;

    
    /**
     * @description static find: model.find()
     * @param {QueryString} query 
     * @return {ModelArray}
     */
    static find(query){
        console.log('calling static find method...', query);
        let results = [];
        let option = {}
        
        //TODO: implement call to sequelize find
        // let option = this.translateQuery(query)
        // results = super.find(option)

        return results
    }

    /**
     * @description instance method: new model().find
     * @param {QueryString} query
     * @return {ModelInstance} 
     */
    find(query){
        this.query = query;
        return this 
    }

    /**
     * @description instance method: 
     * @call new model().populate(fields)
     * @call new model().find(query).populate(fields)
     * @call new model().find(query).populate(fields,cascade=true).sort(sortOption)
     * @param {IncludeFields} fields 
     * @param {Boolean} cascade 
     * @return {ModelArray | ModelInstance} 
     */
    populate(fields, cascade=false){
        console.log('populate :>> ', fields);
        this.includeFields = fields;
        return cascade?this:this.constructor.find(this.query)
    }

    /**
     * @description instance metho for sorting
     * @call new model().sort(sortOption)
     * @call new model().find(query).sort(sortOption)
     * @call new model().find(query).populate(sortOption,cascade:true).sort(sortOption)
     * @param {sortCondition} sort 
     * @param {Boolean} cascade 
     * @return {ModelArray | ModelInstance} 
     */
    sort(conditions, cascade=false){
        this.sortCondition = conditions
        return cascade?this:this.constructor.find(this.query)
    }

     /**
     * @description instance metho for query limit
     * @call new model().limit(limitOption)
     * @call new model().find(query).limit(limitOption)
     * @call new model().find(query).populate(sortOption,cascade:true).limit(limitOption)
     * @param {limitOption} sort 
     * @param {Boolean} cascade 
     */
    limit(option, cascade=false){
        this.limitOption = option
        return cascade?this:this.constructor.find(this.query)
    }

    /**
     * @description instance metho for pagination
     * @call new model().page(pageOption)
     * @call new model().find(query).page(pageOption)
     * @call new model().find(query).populate(sortOption,cascade:true).page(pageOption)
     * @param {pageOption} sort 
     * @param {Boolean} cascade 
     */
    page(option, cascade=false){
        this.pageOption = option
        return cascade?this:this.constructor.find(this.query)
    }

    /**
     * ########################################################
     * Helper Methods
     * ########################################################
     */

     /**
      * @description instance method to translate query options
      * @param {}
      */
     translateQuery(){
        let translated = {}
        // TODO: construct singe query option for sequelize
        // from instance variable (eg. this.query, this.includeFields, etc)

        return translated

     }

     /**
      * 
      * @param {queryOption} query 
      * @param {includeFields} includes 
      * @param {sortCondition} sort 
      * @param {limitOption} limit 
      * @param {pageOption} page 
      */
     static translateQuery(query, includes, sort, limit, page){
        let translated = {}
        // TODO: construct singe query option for sequelize from params

        return translated
     }

    
}