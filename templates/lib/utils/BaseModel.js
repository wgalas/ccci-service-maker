'use strict';
import { Model } from 'sequelize';
import QueryHelper from "./QueryHelper.js"

const FIND_WARNING = '\nWARNING: Calling find() method will not return the results, unless exec() is called (eg. new model().find().exec())\n'
const CHAIN_WARNING = '\nWARNING: Calling chained methods (populate(), sort(), limit()) does not return the result unless exec() is invoked.\n'
const RESET_WARNING = '\nWARNING: This will reset all the query.\n'
const NOT_YET_IMPLEMENTED_WARNING = "\nWARNING: Not yet implemented:"
const FIND_ALL = 'findAndCountAll'
const FIND_PK = 'findByPk'
const FIND_ONE = 'findOne'
const COUNT = 'count'
const RESTORE = 'restore'
const DESTROY = 'destroy'
/**
 * @description BaseModel class
 * @description Mongolize (mongo->sequelize) adaptor
 */
export default class BaseModel extends Model {
    //instance variables
    id;
    findType;
    query;
    includeFields = [];
    sortCondition;
    limitOption;
    selectFields;
    isReturning;
    associations;
    raw = false;
    nest = false;
    isDistinct = false;
    isParanoid = null;

    /**
     * ########################################################
     *                         Find Methods
     * ########################################################
     */

    /**
     * @description findAll
     * @param {QueryString} query
     * @return {ModelInstance}
     */
    find(query) {
        this.resetFields();
        console.warn('\x1b[33m', FIND_WARNING, '\x1b[0m');
        this.query = query;
        this.isDistinct = true;
        console.log('###findquery :>> ', JSON.stringify(query));
        this.findType = FIND_ALL;
        return this;
    };

    /**
     * @description findOne
     * @param {*} query
     */
    findOne(query) {
        this.resetFields();
        console.warn('\x1b[33m', FIND_WARNING, '\x1b[0m');
        this.query = query;
        this.findType = FIND_ONE
        return this;
    };


    /**
     * @description findByPk
     * @param {*} id
     */
    findById(id) {
        this.resetFields();
        console.warn('\x1b[33m', FIND_WARNING, '\x1b[0m');
        // this.id = id
        this.query = { _id: id }
        this.findType = FIND_ONE
        // this.findType = FIND_PK
        return this;
    };

    /**
     * @description equivalent method of sequelize includes
     * @param {IncludeFields} fields
     * @return {ModelInstance}
     */
    populate(fields) {
        console.warn('\x1b[33m', CHAIN_WARNING, '\x1b[0m');
        if (!fields) return this
        if (!Array.isArray(fields)) {
            this.includeFields.push(fields)
        } else {
            this.includeFields.push(...fields)
        }
        return this;
    };

    /**
     * @description
     * @param {DistinctField} bool
     * @return {ModelInstance}
     */
    distinct(bool) {
        console.warn('\x1b[33m', CHAIN_WARNING, '\x1b[0m');
        this.isDistinct = bool === undefined || bool;
        return this;
    };

    /**
     * @description
     * @param {ParanoidField} bool
     * @return {ModelInstance}
     */
    paranoid(bool) {
        console.warn('\x1b[33m', CHAIN_WARNING, '\x1b[0m');
        this.isParanoid = bool == false ? false : true;
        return this;
    };

    /**
     * @description instance method for sorting
     * @param {sortCondition} sort
     * @return {ModelInstance}
     */
    sort(conditions) {
        console.warn('\x1b[33m', CHAIN_WARNING, '\x1b[0m');
        this.sortCondition = conditions
        return this;
    };

    /**
    * @description instance method for query limit
    * @param {limitOption} sort
    * @return {ModelInstance}
    */
    limit(option) {
        console.warn('\x1b[33m', CHAIN_WARNING, '\x1b[0m');
        this.limitOption = option
        return this;
    };

    /**
     * @description instance method for pagination
     * @param {pageOption} sort
     * @return {ModelInstance}
     */
    skip(option) {
        console.warn('\x1b[33m', CHAIN_WARNING, '\x1b[0m');
        this.pageOption = option
        return this;
    };

    /**
     * @description select field function
     * @param {*} fields
     */
    select(fields) {
        console.warn('\x1b[33m', CHAIN_WARNING, '\x1b[0m');
        this.selectFields = fields
        return this;
    }

    /**
     * @description lean to return plain object
     */
    lean() {
        this.raw = true;
        this.nest = true;
        return this;
    }


    /**
     * @description execute find function
     */
    async exec() {
        try {
            // if (this.findType == FIND_PK) return await this.constructor.executeFind(this.id, FIND_PK);
            let options = this.translateQuery();
            return await this.constructor.executeFind(options, this.findType)
        } catch (error) {
            console.log('error :>> ', error);
        }
    };

    /**
     * @description static method to call find method of sequelize
     * @param {queryOption} option
     * @param {String} method
     */
    static async executeFind(option, method) {
        try {
            console.log('!option :>> ', option);
            let result = await super[method](option)
            return result
        } catch (error) {
            console.log('error :>> ', error);
        }

    }


    /**
     * ########################################################
     *                         Create Methods
     * ########################################################
     */

    /**
     * @description create new record
     * @param {Model} data
     */
    async create(data) {
        return await this.constructor.excuteSave(data);
    }

    /**
     * @description save new record
     * @param {Model} data
     */
    // static async save(data) {
    //     return await this.constructor.excuteSave(data);
    // }

    async _save() {
        return await this.constructor.excuteSave(JSON.parse(JSON.stringify(this.dataValues)));
    }

    static async excuteSave(data) {
        try {
            console.log('###excuteSave data :>> ', data);
            // let result = await super.create(data);
            // console.log('results :>> ', results);
            return await super.create(data);
            // return {};
        } catch (error) {
            console.log('error :>> ', error);
            throw error
        }

    }

    static async insertMany(data) {
        try {
            console.log('###insertMany data :>> ', data);
            return await super.bulkCreate(data);
        } catch (error) {
            console.log('error :>> ', error);
            throw error
        }
    }


    /**
     * ########################################################
     *                         Update Methods
     * ########################################################
     */

    async findOneAndUpdate(query, values, options) {
        this.resetFields();

        // construct options
        this.limitOption = 1;
        this.sortCondition = { _id: "DESC" };
        this.cascadeOptions(options);
        this.query = query;
        this.isReturning = true;
        let option = this.translateQuery();

        return await this.constructor.executeUpdate(option, values)
    }

    async findByIdAndUpdate(id, values, options) {
        this.resetFields();

        // construct options
        this.limitOption = 1;
        this.sortCondition = { _id: "DESC" };
        this.cascadeOptions(options);
        this.query = { _id: id };
        this.isReturning = true;
        let option = this.translateQuery();

        return await this.constructor.executeUpdate(option, values)
    }

    async updateMany(query, values, options) {

        // construct options
        this.cascadeOptions(options);
        this.query = query;
        this.isReturning = true;
        let option = this.translateQuery();

        return await this.constructor.executeUpdate(option, values, true)
    }

    /**
     * @description static method to call find method of sequelize
     * @param {queryOption} option
     * @param {String} method
     */
    static async executeUpdate(option, values, isMany) {
        try {
            delete values[this.primaryKeyAttribute]; // to exclude primary key in updating
            values = JSON.parse(JSON.stringify(values)); // need to deepCopy values first to update
            let results = await super.update(values, option);
            // console.log('results :>> ', results);
            if (results && Array.isArray(results) && results[1]) return isMany ? results[1] : results[1][0];
            return results;
        } catch (error) {
            console.log('error :>> ', error);
            throw error
        }

    }

    /**
     * ########################################################
     *                         Delete Methods
     * ########################################################
     */

    async findByIdAndDelete(_id) {
        this.query = { _id };
        let option = this.translateQuery();
        return await this.constructor.executeDelete(option)
    }

    static async executeDelete(option) {
        try {
            return await super.destroy(option);
        } catch (error) {
            console.log('error :>> ', error);
            throw error
        }

    }

    /**
     * ########################################################
     *                         Count Methods
     * ########################################################
     */

    async count(query) {
        this.resetFields();
        this.query = query;
        this.findType = COUNT;
        let options = this.translateQuery();

        return await this.constructor.executeFind(options, this.findType)
    }

    /**
     * ########################################################
     *                         Destroy and Restore Methods
     * ########################################################
     */
    async restore(query) {
        this.findType = RESTORE;
        this.query = query;
        let options = this.translateQuery();
        options.individualHooks = true;
        return await this.constructor.executeFind(options, this.findType)
    }

    async destroy(query, otherOptions = {}) {
        this.findType = DESTROY;
        this.query = query;
        let options = this.translateQuery();
        options = { ...options, ...otherOptions }
        console.log('options :>> ', options);
        return await this.constructor.executeFind(options, this.findType)
    }

    /**
     * ########################################################
     *                         Helper Methods
     * ########################################################
     */

    /**
     * @description instance method to translate query options
     * @param {}
     */
    translateQuery() {
        let translated = {}
        try {
            if (this.query) translated.where = QueryHelper.where(this.query)
            if (this.includeFields && this.includeFields.length > 0) translated.include = QueryHelper.includes(this.includeFields, this.getFields())
            if (this.limitOption) translated.limit = QueryHelper.limit(this.limitOption)
            if (this.pageOption) translated.offset = QueryHelper.offset(this.pageOption)
            if (this.selectFields) translated.attributes = QueryHelper.select(this.selectFields, this.getFields())
            if (this.sortCondition) translated.order = QueryHelper.sort(this.sortCondition)
            if (this.isReturning) translated.returning = true;
            if (this.raw) translated.raw = true;
            if (this.nest) translated.nest = true;
            if (this.isDistinct) translated.distinct = this.isDistinct;
            if (this.isParanoid != null) translated.paranoid = this.isParanoid;
            translated = QueryHelper.handleAssociations(translated, this.getFields(), this.getAssociationsKeys())
            console.log('translated :>> ', this.constructor.name, translated);
        } catch (error) {
            console.error(`ERROR in translating query::: ${error} ${error.lineNumber}`);
        }
        return translated

    };

    getFields() {
        return Object.keys(this.rawAttributes);
    }

    getAssociationsKeys() {
        return this.constructor.associations ? Object.keys(this.constructor.associations) : null;
    }

    /**
     *
     * @param {queryOption} query
     * @param {includeFields} includes
     * @param {sortCondition} sort
     * @param {limitOption} limit
     * @param {pageOption} page
     */
    static translateQuery(query, includes, sort, limit, page) {
        let translated = {}
        // TODO: construct singe query option for sequelize from params
        console.warn('Not yet implemented::: static translateQuery()');
        return translated
    };

    aggregate() {
        console.warn('\x1b[33m', NOT_YET_IMPLEMENTED_WARNING, 'aggregate()', '\x1b[0m');
        return this;
    };

    match() {
        console.warn('\x1b[33m', NOT_YET_IMPLEMENTED_WARNING, 'match()', '\x1b[0m');
        return this;
    };

    group() {
        console.warn('\x1b[33m', NOT_YET_IMPLEMENTED_WARNING, 'group()', '\x1b[0m');
        return this;
    };

    static setAssociations(associations) {
        this.associations = associations;
    }

    resetFields() {
        console.warn('\x1b[33m', RESET_WARNING, '\x1b[0m');
        this.id = undefined;
        this.findType = undefined;
        this.query = undefined;
        this.includeFields = [];
        this.sortCondition = undefined;
        this.limitOption = undefined;
        this.pageOption = undefined;
        this.selectFields = undefined;
        this.isReturning = undefined;
        this.raw = false;
        this.nest = false;
        this.isDistinct = false;
        this.isParanoid = null;
    }

    cascadeOptions(options) {
        if (!options || !Object.keys(options).length) return;
        if (options.populate && ((Array.isArray(options.populate) && options.populate.length) || typeof options.populate == 'string')) {
            console.log('options :>> ', options);
            this.includeFields = Array.isArray(options.populate) ? options.populate : [options.populate];
        }
        if (options.sort) this.sortCondition = options.sort;
        if (options.limit) this.limitOption = options.limit;
        if (options.skip) this.pageOption = options.skip;
        if (options.select) this.selectFields = options.select;
        if (options.lean) {
            this.raw = true;
            this.nest = true;
        }
        if (options.isDistinct) this.isDistinct = true;
        if (options.hasOwnProperty('isParanoid')) this.isParanoid = options.isParanoid;
    }
}
