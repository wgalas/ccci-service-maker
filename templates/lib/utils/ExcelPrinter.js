import { readFileSync } from "fs";
import path from "path";
import XlsxTemplate from "xlsx-template";

import { fileURLToPath } from "url";

const __filename = fileURLToPath(import.meta.url);

const __dirname = path.dirname(__filename);

export default class ExcelPrinter {
  static print(template, data) {
    const file = readFileSync(
      `${__dirname}/../templates/excel/${template}.xlsx`
    );
    var excelTemplate = new XlsxTemplate(file);
    excelTemplate.substitute(1, data);

    return excelTemplate.generate("base64");
  }
}
