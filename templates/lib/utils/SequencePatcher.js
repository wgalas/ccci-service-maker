import fs from "fs";
import path from "path";
import Sequelize from "sequelize";
import enVariables from "../config/db.config.js";
import { fileURLToPath } from "url";

const env = process.env.NODE_ENV || "development";
const config = enVariables[env];
const seeders = [];
const __filename = fileURLToPath(import.meta.url);

const __dirname = path.dirname(__filename);
const basename = "index.js";

async function run() {
  try {
    let sequelize;
    if (config.use_env_variable) {
      sequelize = await new Sequelize(
        process.env[config.use_env_variable],
        config
      );
    } else {
      sequelize = await new Sequelize(
        config.database,
        config.username,
        config.password,
        config
      );
    }
    // for (let table of seeders) {
    //   const [results, metadata] = await sequelize.query(
    //     `SELECT setval('${table}__id_seq', (SELECT MAX("_id") FROM ${table} c)+1)`
    //   );
    //   console.log(`${table} -> ${results[0].setval}`);
    // }

    const files = fs
      .readdirSync(__dirname + "/../models")
      .filter(
        (file) =>
          file.indexOf(".") !== 0 &&
          file !== basename &&
          file.slice(-3) === ".js"
      );

    for (let i = 0; i < files.length; i++) {
      const file = files[i];
      // eslint-disable-next-line global-require,import/no-dynamic-require
      const model = (await import("../models/" + file)).default(
        sequelize,
        Sequelize.DataTypes
      );
      let table = model.tableName;

      try {
        const [results, metadata] = await sequelize.query(
          `SELECT setval('${table}__id_seq', (SELECT MAX("_id") FROM ${table} c)+1)`
        );
        console.log(`${table} -> ${results[0].setval}`);
      } catch (error) {
        console.log(`${table} - ${error}`);
      }
    }

    console.log("finished");
  } catch (error) {
    console.log("run error >> ", error);
  }
}

run();
