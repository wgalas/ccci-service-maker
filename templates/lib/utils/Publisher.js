import redis from "redis";
import Model from "../models/index.js";
const NotificationModel = Model.getDb().notifications;

const REDIS_SERVER = process.env.REDIS_URL || "redis://localhost:6379";
const publisher = process.env.NO_REDIS
  ? { publish: () => true }
  : redis.createClient({ url: REDIS_SERVER });

const ROOT_DIR = "/uis";

const NOTIFICATION_CHANNEL = "service:notifications";
const UPLOAD_CHANNEL = "service:upload";
const BATCH_CHANNEL = "service:batch";

const BROADCAST_CHANNEL = "BROADCAST";
const EMPLOYEE_CHANNEL = "EMPLOYEE:";
const DEPARTMENT_CHANNEL = "DEPARTMENT:";
const DESIGNATION_CHANNEL = "DESIGNATION:";
const LOCAL_DIR = process.env.LOCAL_DIR || "";
// const ASSETS_BASE_URL = process.env.ASSETS_BASE_URL || "";
import ApplicationSettings from "../utils/ApplicationSettings.js";

export default class Publisher {
  static async init() {
    try {
      await publisher.connect();
      publisher.on("error", (error) => {
        console.error(
          "\x1b[31;1m",
          "Local Publisher Error :>> ",
          error,
          "\x1b[0m"
        );
      });
    } catch (error) {
      console.error(
        "\x1b[31;1m",
        "##Local Publisher Error :>> ",
        error,
        "\x1b[0m"
      );
    }
  }

  // /**
  //  * @description Push to upload-service
  //  * @param {Object} data
  //  * @param {String} key
  //  */
  // static async upload(data, key) {
  //     let file = data[key]
  //     if (file) {
  //         file.dir = `${process.env.LOCAL_DIR ? process.env.LOCAL_DIR : ''}${ROOT_DIR}/attachments/${key}`
  //         file.path = `${file.dir}/${(new Date()).getTime()}-${file.name}`
  //         publisher.publish(UPLOAD_CHANNEL, JSON.stringify(file))
  //         return file.path.replace(ROOT_DIR, '');
  //     } else {
  //         return ''
  //     }
  // }

  /**
   * @description Push to upload-service (multi-files)
   * @param {Object} data
   * @param {String} key
   * @param {String} subDir - subdirectory (default = '')
   */
  static async upload(data, subDir = "") {
    let obj = {};
    let keys = Object.keys(data);
    keys.forEach((key) => {
      let files = [];
      let documents = data[key];
      let _files = Array.isArray(documents) ? documents : [documents];
      _files.forEach((file) => {
        file.dir = `${
          LOCAL_DIR ? LOCAL_DIR : ""
        }${ROOT_DIR}/attachments${subDir}/${key}`;
        file.path = `${file.dir}/${new Date().getTime()}-${file.name}`;
        publisher.publish(UPLOAD_CHANNEL, JSON.stringify(file));
        delete file.data;
        delete file.mv;
        file.path =
          ApplicationSettings.getValue("ASSETS_BASE_URL") +
          file.path.replace(ROOT_DIR, "").replace(LOCAL_DIR, "");
        files.push(file);
      });
      obj[key] = files;
    });
    return obj;
  }

  /**
   * @description Push to notification-service
   * @param {*} userId
   * @param {*} data
   */
  static async publish(data) {
    try {
      await publisher.publish(NOTIFICATION_CHANNEL, JSON.stringify(data));
    } catch (error) {
      console.log("publish error >> ", error);
    }
  }

  /**
   *
   * @param {String} channel
   * @param {String} message
   */
  static sendNotifications(channel, message) {
    let data = {
      channel,
      message,
    };
    this.publish(data);
  }

  static sendStudentNotification() {}

  /**
   *
   * @param {String} channel
   * @param {String} message
   */
  static sendBroadCast(message, type) {
    let data = {
      channel: BROADCAST_CHANNEL,
      message,
      type: type || "BROADCAST",
    };
    this.publish(data);

    //save notification
    let notif = new NotificationModel({
      message,
      notificationType: "BROADCAST",
    });

    notif._save();
  }

  /**
   * @description Send Individual EMPLOYEE Notification
   * @param {String} channel
   * @param {String} message
   */
  static async sendEmployeeNotifications(id, message, type) {
    let data = {
      channel: `${EMPLOYEE_CHANNEL}${id}`,
      message,
      type,
    };
    this.publish(data);

    //save notification
    let notif = new NotificationModel({
      employeeId: id,
      message,
      notificationType: type,
    });

    notif._save();
  }

  /**
   * @description Send Notice Notification
   * @param {String} channel
   * @param {String} message
   */
  static async sendNotice(departments, designations, employees, message, type) {
    //BROADCAST
    if (!departments && !designations && !employees) {
      this.sendBroadCast(message, type);
    } else {
      //send by department
      if (departments) {
        departments.forEach((dept) => {
          let data = {
            channel: `${DEPARTMENT_CHANNEL}${dept}`,
            message,
            type,
          };
          this.publish(data);
        });

        //save notification
        let notif = new NotificationModel({
          departments,
          message,
          notificationType: type,
        });
        notif._save();
      }

      //send by department
      if (designations) {
        designations.forEach((pos) => {
          let data = {
            channel: `${DESIGNATION_CHANNEL}${pos}`,
            message,
            type,
          };
          this.publish(data);
        });
        //save notification
        let notif = new NotificationModel({
          designations,
          message,
          notificationType: type,
        });
        notif._save();
      }

      if (employees) {
        employees.forEach((emp) => {
          let data = {
            channel: `${EMPLOYEE_CHANNEL}${emp}`,
            message,
            type,
          };
          this.publish(data);
        });
        //save notification
        let notif = new NotificationModel({
          employees: employees,
          message,
          notificationType: type,
        });
        notif._save();
      }
    }
  }

  static async updateBatch(data) {
    publisher.publish(BATCH_CHANNEL, JSON.stringify(data));
  }
}
