import passport from 'passport';
import BaseRouter from "../../utils/BaseRouter.js";
import ReturnHandling from '../../utils/ReturnHandling.js';
import Controller from "../../controllers/auth.controller.js"

export default class AuthRouter extends BaseRouter {

    constructor() {
        super(new Controller())
    }

    /**
     * @instructions enable snippet to mappings
     */
    getMapping = () => {
        let mappings = [
            // {
            //     method: 'get',
            //     path: '/',
            //     function: this.login,
            //     middleware: passport.authenticate('user-bearer', { session: false })
            // },
            {
                method: 'post',
                path: '/',
                function: this.login,
                middleware: this.loginMiddleware,
                // log: "Successful Login Attempt"
            },
            {
                method: 'post',
                path: '/password/forgot',
                function: 'forgotPassword',
            },
            {
                method: 'post',
                path: '/password/update/:token',
                function: 'setPassword',
            },
            {
                method: 'get',
                path: '/settings/defaults',
                function: 'getDefaultSettings',
            },
            {
                method: 'post',
                path: '/handshake',
                function: this.login,
                middleware: this.tokenExhange
            },
        ];
        return mappings;
    }

    /**
     * @instructions Route Middlewares
     */
    async loginMiddleware(req, res, next) {
        passport.authenticate('user-login', function (error, data, info) {
            if (error || !data) return res.status(401).json(ReturnHandling.retError(error));
            console.log('loginMiddleware::data :>> ', data);
            new Controller().log({ originalUrl: req.originalUrl, user: data }, "LOGIN", "Successful Login Attempt");
            res.json(ReturnHandling.retHandle(200, data, "Successful Login"));
        })(req, res, next);
    }

    async tokenExhange(req, res, next) {
        passport.authenticate('token-exchange', function (error, data, info) {
            if (error || !data) return res.status(401).json(ReturnHandling.retError(error));
            console.log('tokenExhange::data :>> ', data);
            res.json(ReturnHandling.retHandle(200, data, "Successful Login"));
        })(req, res, next);
    }

    /**
     * @instructions Route Functions
     */
    login(req, res) {
        console.log('test login');
        res.sendStatus(200);
    }
}
