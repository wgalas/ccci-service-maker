import BaseRouter from "../../utils/BaseRouter.js";
import Controller from "../../controllers/user.controller.js";
export default class UsersRouter extends BaseRouter {
  constructor() {
    super(new Controller());
  }

  /**
   * @instructions enable snippet to mappings
   */
  getAdditionalMapping = () => {
    let mappings = [
      {
        method: "get",
        path: "/me",
        function: "getUser",
      },
      {
        method: "put",
        path: "/change-password/:id",
        function: "updatePassword",
      },
      {
        method: "post",
        path: "/:id/tokens",
        function: "addNotificationTokens",
      },
      {
        method: "post",
        path: "/:id/unlock",
        function: "unlockUser",
      },
      {
        method: "post",
        path: "/:id/reset-password",
        function: "resetUserPassword",
      },
    ];
    return mappings;
  };
}
