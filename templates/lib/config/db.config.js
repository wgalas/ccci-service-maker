export default {
  development: {
    username: process.env.DEV_DB_USERNAME,
    password: process.env.DEV_DB_PASSWORD,
    database: process.env.DEV_DB_NAME,
    host: process.env.DEV_DB_HOSTNAME,
    port: process.env.DEV_DB_PORT,
    dialect: "postgres",
    logging: false,
    schema: process.env.DEV_DB_SCHEMA,
  },
  qa: {
    username: process.env.QA_DB_USERNAME,
    password: process.env.QA_DB_PASSWORD,
    database: process.env.QA_DB_NAME,
    host: process.env.QA_DB_HOSTNAME,
    port: process.env.QA_DB_PORT,
    dialect: "postgres",
    logging: false,
    schema: process.env.QA_DB_SCHEMA,
  },
  uat: {
    username: process.env.UAT_DB_USERNAME,
    password: process.env.UAT_DB_PASSWORD,
    database: process.env.UAT_DB_NAME,
    host: process.env.UAT_DB_HOSTNAME,
    port: process.env.UAT_DB_PORT,
    dialect: "postgres",
    logging: false,
  },
  staging: {
    username: process.env.STAGING_DB_USERNAME,
    password: process.env.STAGING_DB_PASSWORD,
    database: process.env.STAGING_DB_NAME,
    host: process.env.STAGING_DB_HOSTNAME,
    port: process.env.STAGING_DB_PORT,
    dialect: "postgres",
    logging: false,
  },
  production: {
    username: process.env.PRODUCTION_DB_USERNAME,
    password: process.env.PRODUCTION_DB_PASSWORD,
    database: process.env.PRODUCTION_DB_NAME,
    host: process.env.PRODUCTION_DB_HOSTNAME,
    port: process.env.PRODUCTION_DB_PORT,
    dialect: "postgres",
    logging: false,
  },
};
