"use strict";

import BaseModel from "../utils/BaseModel.js";
import bcrypt from "bcrypt";

export default (sequelize, DataTypes) => {
  class users extends BaseModel {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {}
  }

  users.init(
    {
      username: {
        type: DataTypes.STRING,
        unique: false,
        allowNull: true,
        autoIncrement: false,
        primaryKey: false,
      },
      password: {
        type: DataTypes.STRING,
        unique: false,
        allowNull: true,
        autoIncrement: false,
        primaryKey: false,
      },
      lastLogin: {
        type: DataTypes.DATE,
        unique: false,
        allowNull: true,
        autoIncrement: false,
        primaryKey: false,
      },
      token: {
        type: DataTypes.STRING,
        unique: false,
        allowNull: true,
        autoIncrement: false,
        primaryKey: false,
      },
      isActive: {
        type: DataTypes.BOOLEAN,
        unique: false,
        allowNull: true,
        autoIncrement: false,
        primaryKey: false,
        defaultValue: true,
      },
      _id: {
        type: DataTypes.INTEGER,
        unique: true,
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
      },
      createdById: {
        type: DataTypes.INTEGER,
        unique: false,
        allowNull: true,
        autoIncrement: false,
        primaryKey: false,
      },
      modifiedById: {
        type: DataTypes.INTEGER,
        unique: false,
        allowNull: true,
        autoIncrement: false,
        primaryKey: false,
      },
      accountType: {
        type: DataTypes.ENUM("STUDENT", "EMPLOYEE"),
        unique: false,
        allowNull: true,
        autoIncrement: false,
        primaryKey: false,
      },
      employeeId: {
        type: DataTypes.INTEGER,
        unique: false,
        allowNull: true,
        autoIncrement: false,
        primaryKey: false,
      },
      studentId: {
        type: DataTypes.INTEGER,
        unique: false,
        allowNull: true,
        autoIncrement: false,
        primaryKey: false,
      },
      roleId: {
        type: DataTypes.INTEGER,
        unique: false,
        allowNull: true,
        autoIncrement: false,
        primaryKey: false,
      },
      passwordResetToken: {
        type: DataTypes.STRING,
        unique: false,
        allowNull: true,
        autoIncrement: false,
        primaryKey: false,
      },
      notificationTokens: {
        type: DataTypes.ARRAY(DataTypes.STRING),
        unique: false,
        allowNull: true,
        autoIncrement: false,
        primaryKey: false,
      },
      loginAttempts: {
        type: DataTypes.INTEGER,
        unique: false,
        allowNull: true,
        autoIncrement: false,
        primaryKey: false,
      },
      lockedDate: {
        type: DataTypes.DATE,
        unique: false,
        allowNull: true,
        autoIncrement: false,
        primaryKey: false,
      },
    },
    {
      createdAt: "createdDate",
      updatedAt: "modifiedDate",
      sequelize,
      modelName: "users",
    }
  );

  users.beforeCreate(async (user, options) => {
    user.createdDate = new Date();
    user.isActive = true;
    if (user.password) {
      const salt = bcrypt.genSaltSync(5);
      const hash = bcrypt.hashSync(user.password, salt);
      user.password = hash;
    }
  });

  //   users.beforeSave(async (user, options) => {
  //         user.modifiedDate = new Date()
  //         if (user.password) {
  //               const salt = bcrypt.genSaltSync(5);
  //               const hash = bcrypt.hashSync(user.password, salt)
  //               user.password = hash;
  //         }
  //   })

  users.prototype.encryptPassword = function (password) {
    const salt = bcrypt.genSaltSync(5);
    return bcrypt.hashSync(password, salt);
  };

  users.prototype.isValid = async function (password) {
    return await bcrypt.compare(password, this.password);
  };

  return users;
};
